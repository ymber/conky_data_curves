require "cairo"

rings = {}

function draw_ring( context, ring, percent )
    local filled_arc_length = ( ring["end_angle"] - ring["start_angle"] ) * percent

    -- Draw arc background
    cairo_set_source_rgba( context, unpack( ring["bg_rgba"] ) )
    cairo_arc( context, ring["x"], ring["y"], ring["radius"], ring["start_angle"], ring["end_angle"] )
    cairo_set_line_width( context, ring["thickness"] )
    cairo_stroke( context )

    -- Draw arc foreground
    cairo_set_source_rgba( context, unpack( ring["fg_rgba"] ) )
    cairo_arc( context, ring["x"], ring["y"], ring["radius"], ring["start_angle"], ring["start_angle"] + filled_arc_length )
    cairo_stroke( context )

end

function ring_data(ring)
    -- Returns the percent used value for the ring's conky variable and argument
    local value_str = conky_parse( string.format( "${%s %s}", ring[ "name" ], ring[ "arg" ] ) )
    return tonumber( value_str ) / ring[ "max" ]
end

function conky_rings()
    if conky_window == nil then return end

    local cairo_surface = cairo_xlib_surface_create( conky_window.display,
                                                     conky_window.drawable,
                                                     conky_window.visual,
                                                     conky_window.width,
                                                     conky_window.height )
    local cairo_context = cairo_create( cairo_surface )

    for i in pairs( rings ) do
        draw_ring( cairo_context, rings[i], ring_data( rings[i] ) )
    end
end
