# Conky data curves

This script draws rings and arcs showing values for the specified conky variables.

## Ring format

```lua
{
    name = "cpu",
    arg = "cpu1",
    max = 100,
    bg_rgba = {1, 1, 1, 0.2},
    fg_rgba = {1, 1, 1, 0.5},
    x = 110,
    y = 160,
    radius = 75,
    thickness = 5,
    start_angle = -( math.pi / 2 ),
    end_angle = math.pi * ( 3 / 2 )
}
```

The `rings` table contains definitions for all the rings the script will render. The above example shows the percent utilization of CPU core 1, displayed clockwise in a circle from the positive y axis. A ring can be a full circle or an arc of any length. All angles are measured in radians clockwise from the positive x axis. Arcs are rendered in the direction of increasing angle.

* `name` is the conky variable the ring will display info for.
* `arg` is the conky variable's argument.
* `max` is the maximum value of the data the ring will display. `max` should be 100 for percentages.
* `bg_rgba` is a table containing the rgba values for the ring's background arc. Its elements should be red, green, blue, and alpha in that order.
* `fg_rgba` is a table containing the rgba values for the ring's foreground arc. Its elements should be red, green, blue, and alpha in that order.
* `x` is the x coordinate of the ring's center.
* `y` is the y coordinate of the ring's center.
* `radius` is the radius of the ring.
* `thickness` is the thickness of the ring's edge.
* `start_angle` is the angle at the start of the arc.
* `end_angle` is the angle at the end of the arc.

## Calling from conky

```lua
lua_load = "/PATH/TO/SCRIPT",
lua_draw_hook_pre = "rings",
```

Inserting these lines in the conky.config table of your conky configuration file will call conky_rings() every time the conky window refreshes.